use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day02(&filename).unwrap();
}

#[derive(Debug, Clone)]
enum SubmarineCommand {
    Forward(i64),
    Up(i64),
    Down(i64),
}
impl From<&String> for SubmarineCommand {
    fn from(input: &String) -> Self {
        use SubmarineCommand::*;
        let mut parts = input.split_ascii_whitespace();
        let direction = parts.next().unwrap();
        let magnitude = parts.next().unwrap().parse::<i64>().unwrap();
        match direction {
            "forward" => Forward(magnitude),
            "up"      => Up(magnitude),
            "down"    => Down(magnitude),
            other     => panic!("Unknown direction: {}", other),
        }
    }
}

fn day02(input: &str) -> io::Result<()> {
    use SubmarineCommand::*;
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    // Input
    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    
    let commands = input
        .iter()
        .map(SubmarineCommand::from)
        .collect::<Vec<SubmarineCommand>>();

    // Part 1
    let mut depth = 0;
    let mut pos = 0;
    for cmd in commands.clone() {
        match cmd {
            Forward(mag) => { pos += mag },
            Up(mag)      => { depth -= mag },
            Down(mag)    => { depth += mag }, 
        }
    }
    println!("Part 1: {}", depth * pos); // 1893605

    // Part 2
    let mut depth = 0;
    let mut pos = 0;
    let mut aim = 0;
    for cmd in commands.clone() {
        match cmd {
            Forward(mag) => { pos += mag; depth += aim * mag },
            Up(mag)      => { aim -= mag },
            Down(mag)    => { aim += mag },
        }
    }
    println!("Part 2: {}", depth * pos); // 2120734350

    // Part 2 (with .fold() for the novelty)
    let part2 = commands
        .clone()
        .iter()
        .fold((0,0,0), |(depth,pos,aim),cmd| {
            match cmd {
                Forward(mag) => (depth + aim*mag, pos + mag, aim),
                Up(mag)      => (depth          , pos      , aim - mag),
                Down(mag)    => (depth          , pos      , aim + mag),
            }
        });
    println!("Part 2: {}", part2.0 * part2.1); // 2120734350

    Ok(())
}

