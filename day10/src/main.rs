use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::collections::VecDeque;

fn corresponding(delim: char) -> char {
    match delim {
        '(' => ')',
        '[' => ']',
        '{' => '}',
        '<' => '>',
        other => panic!("Syntax error: unexpected character: {}",other),
    }
}

fn delim_score_p1(delim: char) -> usize {
    match delim {
        ')' => 3,
        ']' => 57,
        '}' => 1197,
        '>' => 25137,
        other => panic!("Syntax error: unexpected character: {}",other),
    }
}

fn delim_score_p2(delim: char) -> usize {
    match delim {
        ')' => 1,
        ']' => 2,
        '}' => 3,
        '>' => 4,
        other => panic!("Syntax error: unexpected character: {}",other),
    }
}

fn score_line_p1(line: &str) -> usize {
    let mut stack: VecDeque<char> = VecDeque::new();
    for ch in line.chars() {
        match ch {
            '(' | '[' | '{' | '<' => stack.push_front(ch),
            ')' | ']' | '}' | '>' => {
                let next = stack.pop_front().unwrap();
                if ch != corresponding(next) {
                    return delim_score_p1(ch); // corrupted line
                }
            },
            other => panic!("Syntax error: unexpected character: {}",other),
        }
    }
    0 // incomplete line
}

fn complete_line(line: &str) -> String {
    let mut completion = String::new();
    let mut stack: VecDeque<char> = VecDeque::new();
    // Parse existing string
    for ch in line.chars() {
        match ch {
            '(' | '[' | '{' | '<' => { stack.push_front(ch) },
            ')' | ']' | '}' | '>' => { stack.pop_front(); },
            other => panic!("Syntax error: unexpected character: {}",other),
        }
    }
    // Complete string
    while stack.len() > 0 {
        let next = stack.pop_front().unwrap();
        let corresponding = corresponding(next);
        completion.push_str(&corresponding.to_string());
    }
    completion
}

fn score_line_p2(completion: String) -> usize {
    completion.chars().fold(0, |score, ch| score * 5 +  delim_score_p2(ch))
}

fn solve(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    // Input
    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    // Part 1
    let part1: usize = input.clone().iter().map(|x| score_line_p1(x)).sum();
    println!("Part 1: {}", part1); // 392043

    // Part 2
    let incomplete_lines: Vec<_> = input
        .iter()
        .filter(|x| score_line_p1(x) == 0)
        .collect();
    let mut part2_scores = incomplete_lines
        .iter()
        .map(|x| complete_line(x))
        .map(|x| score_line_p2(x))
        .collect::<Vec<_>>();
    part2_scores.sort();
    let middle_index = part2_scores.len()/2;
    let part2 = part2_scores.iter().nth(middle_index).unwrap();
    println!("Part 2: {}", part2); // 1605968119

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    solve(&filename).unwrap();
}
