use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::fmt;

const DEBUG: bool = false;

struct Point {
    x: usize,
    y: usize,
}

type SeaCucumberMap = Vec<Vec<Option<SeaCucumber>>>;

#[derive(Clone,Copy,PartialEq)]
enum SeaCucumber {
    East,
    South,
}
impl fmt::Display for SeaCucumber {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            SeaCucumber::East => write!(f, ">"),
            SeaCucumber::South => write!(f, "v"),
        }
    }
}

fn solve(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    // Input
    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    // Build sea cucumber map
    let mut cucumber_map: SeaCucumberMap = Vec::new();
    for line in input {
        let mut new_line = Vec::new();
        for ch in line.chars() {
            match ch {
                'v' => new_line.push(Some(SeaCucumber::South)),
                '>' => new_line.push(Some(SeaCucumber::East)),
                '.' => new_line.push(None),
                _ => unreachable!(),
            }
        }
        cucumber_map.push(new_line);
    }

    let (xmax,ymax) = map_limits(&cucumber_map);

    // Simulate
    'main_lp: for step in 0.. {

        // Debug
        if DEBUG { print_map(&cucumber_map, Some(step)); }

        // Consider east moves
        let map_before = cucumber_map.clone();
        let mut movable_east = Vec::new();
        for (y,row) in map_before.iter().enumerate() {
            for (x,cuke) in row.iter().enumerate() {
                if *cuke != Some(SeaCucumber::East) { continue }
                let pt = Point{x,y};
                if let Some(dest) = cucumber_destination(&pt, SeaCucumber::East, xmax, ymax, &map_before) {
                    movable_east.push((pt,dest));
                }
            }
        }

        // Perform east moves
        for (pt,dest) in movable_east {
            cucumber_map[pt.y][pt.x] = None;
            cucumber_map[dest.y][dest.x] = Some(SeaCucumber::East);
        }

        // Consider south moves
        let map_before2 = cucumber_map.clone();
        let mut movable_south = Vec::new();
        for (y,row) in map_before.iter().enumerate() {
            for (x,cuke) in row.iter().enumerate() {
                if *cuke != Some(SeaCucumber::South) { continue }
                let pt = Point{x,y};
                if let Some(dest) = cucumber_destination(&pt, SeaCucumber::South, xmax, ymax, &map_before2) {
                    movable_south.push((pt,dest));
                }
            }
        }

        // Perform south moves
        for (pt,dest) in movable_south {
            cucumber_map[pt.y][pt.x] = None;
            cucumber_map[dest.y][dest.x] = Some(SeaCucumber::South);
        }

        // Check for movement
        if map_before == cucumber_map {
            println!("Part 1: {}", step+1); // 509
            break 'main_lp;
        }
    }

    Ok(())
}

fn cucumber_destination(pt: &Point, cucumber: SeaCucumber, xmax: usize, ymax: usize, map: &SeaCucumberMap) -> Option<Point> {
    let newx = if pt.x+1 < xmax { pt.x+1 } else { 0 };
    let newy = if pt.y+1 < ymax { pt.y+1 } else { 0 };
    let dest = match cucumber {
        SeaCucumber::East  => Point { x: newx, y: pt.y },
        SeaCucumber::South => Point { x: pt.x, y: newy },
    };
    if !map[dest.y as usize][dest.x as usize].is_some() {
        return Some(dest);
    }
    None
}

fn map_limits(map: &SeaCucumberMap) -> (usize,usize) {
    (map[0].len(),map.len())
}

fn print_map(map: &SeaCucumberMap, step: Option<usize>) {
    let (xmax,ymax) = map_limits(&map);
    if step.is_some() { println!("Step: {}", step.unwrap()); }
    for y in 0..ymax {
        for x in 0..xmax {
            match map[y][x] {
                Some(cuke) => print!("{}", cuke),
                None       => print!("."),
            }
        }
        println!();
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    solve(&filename).unwrap();
}
