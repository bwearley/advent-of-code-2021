use std::env;
use std::io::{self};

fn crab_fuel_part1(crab: isize, destination: isize) -> isize {
    (crab - destination).abs()
}

fn crab_fuel_part2(crab: isize, destination: isize) -> isize {
    let x = (crab - destination).abs();
    (x.pow(2) + x) / 2
}

fn part1(input: &Vec<isize>) -> isize {
    let (min,max) = (input.iter().min().unwrap(), input.iter().max().unwrap());
    /*let mut min_fuel = isize::MAX;
    for dest in *min..=*max {
        let fuel = input.iter().map(|crab| crab_fuel_part1(*crab,dest)).sum();
        min_fuel = std::cmp::min(min_fuel, fuel);
    }
    min_fuel*/               // Refactored to be more Rusty (but less clear)
    (*min..=*max)
        .into_iter()
        .map(|dest| input.iter().map(|crab| crab_fuel_part1(*crab,dest)).sum())
        .min()
        .unwrap()
}

fn part2(input: &Vec<isize>) -> isize {
    let (min,max) = (input.iter().min().unwrap(), input.iter().max().unwrap());
    /*let mut min_fuel = isize::MAX;
    for dest in *min..=*max {
        let fuel = input.iter().map(|crab| crab_fuel_part2(*crab,dest)).sum();
        min_fuel = std::cmp::min(min_fuel, fuel);
    }
    min_fuel*/               // Refactored to be more Rusty (but less clear)
    (*min..=*max)
        .into_iter()
        .map(|dest| input.iter().map(|crab| crab_fuel_part2(*crab,dest)).sum())
        .min()
        .unwrap()
}

fn day07(input: &str) -> io::Result<()> {
    // Input
    let input = std::fs::read_to_string(input)
        .unwrap()
        .trim()
        .split(",")
        .map(|x| x.parse::<isize>().unwrap())
        .collect();

    // Answers
    println!("Part 1: {}", part1(&input)); // 349812
    println!("Part 2: {}", part2(&input)); // 99763899

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day07(&filename).unwrap();
}
