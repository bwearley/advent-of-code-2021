use std::env;
use std::io::{self};

const CARDSIZE: usize = 5;

#[derive(Clone)]
struct BingoCard {
    pub card: Vec<Vec<usize>>,
    pub marked: Vec<Vec<bool>>,
    pub won: bool,
}
impl From<&str> for BingoCard {
    fn from(s: &str) -> Self {
        let mut card: Vec<Vec<usize>> = Vec::new();
        for row in s.split("\n") {
            card.push(row.split_ascii_whitespace().map(|x| x.parse::<usize>().unwrap()).collect());
        }
        Self { card: card, marked: vec![vec![false;CARDSIZE];CARDSIZE], won: false }
    }
}
impl BingoCard {
    pub fn validate(&mut self) -> bool {
        /*for i in 0..CARDSIZE {
            if self.marked[i][..].iter().all(|&x| x) { self.won = true; return true; }
            if self.marked[..][i].iter().all(|&x| x) { self.won = true; return true; }
        }*/
        for row in 0..CARDSIZE {
            let mut bingo = true;
            for col in 0..CARDSIZE {
                if !self.marked[row][col] { bingo = false; }
            }
            if bingo { self.won = true; }
        }
        for col in 0..CARDSIZE {
            let mut bingo = true;
            for row in 0..CARDSIZE {
                if !self.marked[row][col] { bingo = false; }
            }
            if bingo { self.won = true; }
        }
        self.won
    }
    pub fn score(&self) -> usize {
        let mut score = 0;
        for row in 0..CARDSIZE {
            for col in 0..CARDSIZE {
                if !self.marked[row][col] { score += self.card[row][col]; }
            }
        }
        if self.won { score } else { 0 }
    }
    pub fn mark_number(&mut self, number: usize) {
        'outer: for (i,row) in self.card.iter().enumerate() {
            for (j,col) in row.iter().enumerate() {
                if col == &number {
                    self.marked[i][j] = true;
                    break 'outer;
                }
            }
        }
    }
}

fn day04(input: &str) -> io::Result<()> {
    // Input
    let input_str = std::fs::read_to_string(input).unwrap();
    let input_str = input_str.trim();
    let input: Vec<_> = input_str.split("\n\n").collect();
    
    // Get Bingo numbers and cards
    let drawn: Vec<_> = input[0].split(",").map(|x| x.parse::<usize>().unwrap()).collect();
    let mut cards: Vec<_> = input[1..].into_iter().map(|x| BingoCard::from(*x)).collect();
    let num_bingo_cards = cards.len();

    // Play Bingo
    let mut bingos = 0;
    'bingo_loop: for number in drawn {
        for card in &mut cards {
            if card.won { continue; }
            card.mark_number(number);
            // Handle wins
            if card.validate() {
                bingos += 1;
                if bingos == 1 {
                    println!("Part 1: {}", number * card.score()); // 89001
                } else if bingos == num_bingo_cards {
                    println!("Part 2: {}", number * card.score()); // 7296
                    break 'bingo_loop;
                }
            }
        }
    }
    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day04(&filename).unwrap();
}