use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::collections::{HashMap,HashSet};

use point2d::point2d::Point2D;

fn neighbor_addresses(pt: &Point2D) -> Vec<Point2D> {
    vec![Point2D { x: pt.x  , y: pt.y-1 },  // up
         Point2D { x: pt.x+1, y: pt.y   },  // right
         Point2D { x: pt.x  , y: pt.y+1 },  // down
         Point2D { x: pt.x-1, y: pt.y   }]  // left
}

fn neighbor_values(pt: &Point2D, map: &HashMap<Point2D,i64>) -> Vec<i64> {
    let neighbors = neighbor_addresses(&pt);
    let mut values: Vec<i64> = Vec::new();
    for neighbor in neighbors {
        match map.get(&neighbor) {
            Some(value) => values.push(*value),
            None => {},
        }
    }
    values
}

fn is_low(pt: Point2D, map: &HashMap<Point2D,i64>) -> bool {
    let neighbor_values = neighbor_values(&pt, &map);
    map.get(&pt).unwrap() < neighbor_values.iter().min().unwrap()
}

fn uphill_neighbors(pt: Point2D, map: &HashMap<Point2D,i64>) -> Vec<Point2D> {
    let this_value = map.get(&pt).unwrap();
    let neighbors = neighbor_addresses(&pt);
    let mut uphill: Vec<Point2D> = Vec::new();
    for neighbor in neighbors {
        if let Some(value) = map.get(&neighbor) {
            if value > this_value && value < &9 {
                uphill.push(neighbor);
                for n in uphill_neighbors(neighbor,&map) { uphill.push(n); }
            }
        }
    }
    uphill
}

fn risk_level(pt: Point2D, map: &HashMap<Point2D,i64>) -> i64 {
    map.get(&pt).unwrap() + 1
}

fn map_extents(map: &HashMap<Point2D,i64>) -> (i64,i64,i64,i64) {
    let xmin = &map.keys().map(|&pt| pt.x).min().unwrap();
    let ymin = &map.keys().map(|&pt| pt.y).min().unwrap();
    let xmax = &map.keys().map(|&pt| pt.x).max().unwrap();
    let ymax = &map.keys().map(|&pt| pt.y).max().unwrap();
    (*xmin,*ymin,*xmax,*ymax)
}

fn solve(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    // Input
    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    // Build floor map
    let mut floor_map: HashMap<Point2D,i64> = HashMap::new();
    for (y,line) in input.iter().enumerate() {
        for (x,ch) in line.chars().enumerate() {
            let (x,y,v) = (x as i64, y as i64, ch.to_digit(10).unwrap());
            floor_map.insert(Point2D{x,y},v.into());
        }
    }

    // Part 1
    let mut part1: i64 = 0;
    let mut low_points: Vec<Point2D> = Vec::new();
    let (xmin,ymin,xmax,ymax) = map_extents(&floor_map);
    for y in ymin..=ymax {
        for x in xmin..=xmax {
            let pt = Point2D { x: x, y: y };
            if is_low(pt,&floor_map) {
                low_points.push(pt);
                part1 += risk_level(pt,&floor_map);
            }
        }
    }
    println!("Part 1: {}", part1); // 575

    // Part 2
    let mut basin_sizes: Vec<usize> = Vec::new();
    for basin_bottom in low_points {
        let basin = uphill_neighbors(basin_bottom,&floor_map);
        let this_basin: HashSet<Point2D> = HashSet::from_iter(basin); // HashSet to dedup
        basin_sizes.push(this_basin.len() + 1); // +1 accounts for the starting point
    }
    basin_sizes.sort_by(|a,b| b.cmp(a)); // descending sort
    let part2: usize = basin_sizes.iter().take(3).product();
    println!("Part 2: {}", part2); // 1019700

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    solve(&filename).unwrap();
}
