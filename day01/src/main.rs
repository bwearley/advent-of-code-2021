use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

fn sonar(input: &Vec<i64>, winsize: usize) -> usize {
    input
        .windows(winsize)
        .map(|x| x.iter().sum::<i64>())
        .collect::<Vec<_>>()
        .windows(2)
        .filter(|x| x[1] > x[0])
        .count()
}

fn day01(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    // Input
    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let input: Vec<i64> = input
        .iter()
        .map(|x| x.parse::<i64>().unwrap())
        .collect();

    // Solutions
    println!("Part 1: {}", sonar(&input, 1)); // 1301
    println!("Part 2: {}", sonar(&input, 3)); // 1346

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day01(&filename).unwrap();
}