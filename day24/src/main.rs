use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::collections::HashMap;

#[derive(Copy, Clone)]
enum Op {
    Inp(usize),
    Add(usize,Value),
    Mul(usize,Value),
    Div(usize,Value),
    Mod(usize,Value),
    Eql(usize,Value),
}
impl From<&String> for Op {
    fn from(s: &String) -> Self {
        let parts: Vec<_> = s.split_ascii_whitespace().collect();
        let a = match parts[1] {
            "w" => 0,
            "x" => 1,
            "y" => 2,
            "z" => 3,
            _ => unreachable!(),
        };
        let b = if parts.len() == 3 {
            match parts[2] {
                "w" => Value::Reg(0),
                "x" => Value::Reg(1),
                "y" => Value::Reg(2),
                "z" => Value::Reg(3),
                v   => Value::Const(v.parse().unwrap()),
            }
        } else { Value::Const(0) };
        match parts[0] {
            "inp" => Self::Inp(a),
            "add" => Self::Add(a,b),
            "mul" => Self::Mul(a,b),
            "div" => Self::Div(a,b),
            "mod" => Self::Mod(a,b),
            "eql" => Self::Eql(a,b),
            _ => unreachable!(),
        }
    }
}

#[derive(Copy, Clone)]
enum Value {
    Const(i64),
    Reg(usize),
}

#[derive(Copy, Clone, Eq, PartialEq, Hash)]
struct ALUSystem {
    reg: [i64; 4]
}
impl ALUSystem {
    fn new() -> Self {
        Self { reg: [0; 4] }
    }
    fn get(&self, value: Value) -> i64 {
        match value {
            Value::Const(v) => v,
            Value::Reg(r) => self.reg[r],
        }
    }
    fn set(&mut self, reg: usize, value: Value) {
        self.reg[reg] = self.get(value)
    }
    fn process(&mut self, instr: Op) {
        use Op::*;
        match instr {
            Inp(_)   => unreachable!(), // Handled in dedicated inp() function
            Add(a,b) => self.reg[a] += self.get(b),
            Mul(a,b) => self.reg[a] *= self.get(b),
            Div(a,b) => self.reg[a] /= self.get(b),
            Mod(a,b) => self.reg[a] %= self.get(b),
            Eql(a,b) => {
                let b = self.get(b);
                self.reg[a] = if self.reg[a] == b { 1 } else { 0 }
            },
        }
    }
    fn inp(&mut self, instr: Op, value: i64) {
        match instr {
            Op::Inp(a) => self.set(a, Value::Const(value)),
            _ => unreachable!(),
        }
    }
}

#[derive(Copy, Clone)]
struct MONADResult {
    sys: ALUSystem,
    min: i64,
    max: i64,
}

fn solve(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    // Input
    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let instr: Vec<_> = input.iter().map(Op::from).collect();

    let mut systems: Vec<MONADResult> = Vec::new();
    systems.push(MONADResult { sys: ALUSystem::new(), min: 0, max: 0 });
    for op in instr {
        println!("Processing {} ALUSystems", systems.len());
        match op {
            Op::Inp(_) => {
                // Create new ALUSystem with every possible digit
                let mut new_systems: Vec<MONADResult> = Vec::new();
                let mut system_index: HashMap<ALUSystem,i64> = HashMap::new();
                for digit in 1..=9 {
                    for sys in &mut systems {
                        let mut new = sys.clone();
                        new.sys.reg[0] = 0; // clear wxy registers
                        new.sys.reg[1] = 0; // clear wxy registers
                        new.sys.reg[2] = 0; // clear wxy registers
                        new.sys.inp(op, digit);
                        new.min = new.min*10 + digit;
                        new.max = new.max*10 + digit;
                        match system_index.get(&new.sys) {
                            Some(v) => {
                                // Previously seen -- update min and max on last seen ALU
                                let v = *v as usize;
                                new_systems[v].min = i64::min(new_systems[v].min, new.min);
                                new_systems[v].max = i64::max(new_systems[v].max, new.max);
                            },
                            None => {
                                // New ALU -- insert
                                system_index.insert(new.sys, new_systems.len() as i64);
                                new_systems.push(new);
                            }
                        }
                    }
                }
                systems = new_systems;
            },
            op => {
                for sys in &mut systems {
                    sys.sys.process(op);
                }
            }
        }
    }

    let part1 = systems.iter().filter(|s| s.sys.reg[3] == 0).map(|s| s.max).max().unwrap();
    let part2 = systems.iter().filter(|s| s.sys.reg[3] == 0).map(|s| s.min).min().unwrap();

    println!("Part 1: {}", part1); // 99911993949684
    println!("Part 2: {}", part2); // 62911941716111


    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    solve(&filename).unwrap();
}
