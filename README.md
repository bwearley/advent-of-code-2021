# Advent of Code 2021

Day #  | Part 1 | Part 2 | Notes
:---:  | :----: | :---: | :---:
Day 1  | :heavy_check_mark: | :heavy_check_mark: | N/A 
Day 2  | :heavy_check_mark: | :heavy_check_mark: | N/A 
Day 3  | :heavy_check_mark: | :heavy_check_mark: | N/A 
Day 4  | :heavy_check_mark: | :heavy_check_mark: | N/A 
Day 5  | :heavy_check_mark: | :heavy_check_mark: | N/A 
Day 6  | :heavy_check_mark: | :heavy_check_mark: | N/A
Day 7  | :heavy_check_mark: | :heavy_check_mark: | N/A
Day 8  | :heavy_check_mark: | :heavy_check_mark: | N/A
Day 9  | :heavy_check_mark: | :heavy_check_mark: | N/A
Day 10 | :heavy_check_mark: | :heavy_check_mark: | N/A
Day 11 | :heavy_check_mark: | :heavy_check_mark: | N/A
Day 12 | :heavy_check_mark: | :heavy_check_mark: | N/A
Day 13 | :heavy_check_mark: | :heavy_check_mark: | N/A
Day 14 | :heavy_check_mark: | :heavy_check_mark: | N/A
Day 15 | :heavy_check_mark: | :heavy_check_mark: | N/A
Day 16 | :heavy_check_mark: | :heavy_check_mark: | N/A
Day 17 | :heavy_check_mark: | :heavy_check_mark: | Sloppy brute-force code
Day 18 | :heavy_check_mark: | :heavy_check_mark: | N/A
Day 19 | :heavy_check_mark: | :heavy_check_mark: | N/A
Day 20 | :heavy_check_mark: | :heavy_check_mark: | N/A
Day 21 | :heavy_check_mark: | :heavy_check_mark: | N/A
Day 22 | :heavy_check_mark: | :heavy_check_mark: | N/A
Day 23 | :heavy_check_mark: | :heavy_check_mark: | Slow solution (part 2: 15 sec)
Day 24 | :heavy_check_mark: | :heavy_check_mark: | Very slow (9 min)
Day 25 | :heavy_check_mark: | :heavy_check_mark: | N/A
