use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::collections::HashMap;

extern crate regex;
use regex::Regex;

#[derive(Clone,Hash,PartialEq,Eq)]
struct Point {
    x: isize,
    y: isize,
}

#[derive(Clone)]
struct Line {
    start: Point,
    end: Point,
}
impl From<&String> for Line {
    fn from(s: &String) -> Self {
        let re = Regex::new(r"(\d+),(\d+) -> (\d+),(\d+)").unwrap();
        let matches = re.captures(s).unwrap();
        Self {
            start: Point { x: matches[1].parse().unwrap(), y: matches[2].parse().unwrap() },
            end:   Point { x: matches[3].parse().unwrap(), y: matches[4].parse().unwrap() },
         }
    }
}
impl Line {
    pub fn points(&self, diagonals: bool) -> Vec<Point> {
        let mut points = Vec::new();
        let dx = self.end.x-self.start.x;
        let dy = self.end.y-self.start.y;
        let delta = if dx != 0 { dx } else { dy };
        if !diagonals && dx != 0 && dy != 0 { return points; } // part1: only vertical or horizontal lines
        let dx = dx.signum();
        let dy = dy.signum();
        for i in 0..delta.abs() + 1 {
            points.push(Point { x: self.start.x + i * dx, y: self.start.y + i * dy });
        }
        points
    }
}

fn day05(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    // Input
    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let lines: Vec<_> = input.iter().map(Line::from).collect();

    // Part 1 (using nested Vecs)
    let mut vent_map = vec![vec![0; 1000]; 1000];
    for line in lines.clone() {
        for pt in line.points(false) {
            vent_map[pt.x as usize][pt.y as usize] += 1;
        }
    }
    let part1 = vent_map.iter().flat_map(|x| x.iter().filter(|&x| *x >= 2)).count();    
    println!("Part 1: {}", part1); // 6461

    // Part 2 (using HashMap)
    let mut vent_map: HashMap<Point,usize> = HashMap::new();
    for line in lines.clone() {
        for pt in line.points(true) {
            *vent_map.entry(pt).or_insert(0) += 1;
        }
    }
    let part2 = vent_map.iter().filter(|(_,&num)| num >= 2).count();
    println!("Part 2: {}", part2); // 18065

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day05(&filename).unwrap();
}
