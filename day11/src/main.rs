use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use point2d::point2d::Point2D;

const GRID_SIZE: usize = 10;
const DEBUG: bool = false;

const PART_1_STEPS: usize = 100;

fn neighbor_addresses(pt: &Point2D) -> Vec<Point2D> {
    let mut neighbors = 
    vec![Point2D { x: pt.x  , y: pt.y-1 },  // up
         Point2D { x: pt.x+1, y: pt.y   },  // right
         Point2D { x: pt.x  , y: pt.y+1 },  // down
         Point2D { x: pt.x+1, y: pt.y+1 },  // right down
         Point2D { x: pt.x-1, y: pt.y+1 },  // left down
         Point2D { x: pt.x+1, y: pt.y-1 },  // right up
         Point2D { x: pt.x-1, y: pt.y-1 },  // left up
         Point2D { x: pt.x-1, y: pt.y   }]; // left
    neighbors.retain(|&pt| pt.x >= 0 && pt.y >= 0);
    neighbors.retain(|&pt| pt.x < GRID_SIZE as i64 && pt.y < GRID_SIZE as i64);
    neighbors
}

fn solve(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    // Input
    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    // Build octopus map
    let mut oct_map: Vec<Vec<u32>> = vec![vec![0; GRID_SIZE]; GRID_SIZE];
    let mut flash_map: Vec<Vec<usize>> = vec![vec![usize::MAX; GRID_SIZE]; GRID_SIZE];
    for (y,line) in input.iter().enumerate() {
        for (x,ch) in line.chars().enumerate() {
            oct_map[x][y] = ch.to_digit(10).unwrap();
        }
    }

    // Simulate
    let mut flashes: usize = 0;
    'main_lp: for step in 0.. {

        // Part 1
        if step == PART_1_STEPS {
            println!("Part 1: {}", flashes); // 1691
        }

        // Part 2
        if oct_map.iter().flatten().sum::<u32>() == 0 {
            println!("Part 2: {}", step); // 216
            break 'main_lp;
        }

        // Debug
        if DEBUG {
            println!("Step: {}", step);        
            for y in 0..GRID_SIZE {
                for x in 0..GRID_SIZE {
                    print!("{}", oct_map[x][y]);
                }
                println!();
            }
        }
        
        // First, the energy level of each octopus increases by 1.
        for y in 0..GRID_SIZE {
            for x in 0..GRID_SIZE {
                oct_map[x][y] += 1;
            }
        }

        // Then, any octopus with an energy level greater than 9 flashes.
        'inner: loop {
            let mut changes = 0;
            for y in 0..GRID_SIZE {
                for x in 0..GRID_SIZE {
                    if oct_map[x][y] > 9 && flash_map[x][y] != step {
                        flash_map[x][y] = step;
                        flashes += 1;
                        changes += 1;

                        // This increases the energy level of all adjacent octopuses by 1,
                        // including octopuses that are diagonally adjacent.
                        let pt = Point2D { x: x as i64, y: y as i64 };
                        for neighbor in neighbor_addresses(&pt) {
                            let (newx,newy) = (neighbor.x as usize, neighbor.y as usize);
                            oct_map[newx][newy] += 1;
                        }
                    }
                }
            }
            if changes == 0 { break 'inner; }
        }

        // Finally, any octopus that flashed during this step has its energy
        // level set to 0, as it used all of its energy to flash.
        for y in 0..GRID_SIZE {
            for x in 0..GRID_SIZE {
                if flash_map[x][y] == step {
                    oct_map[x][y] = 0;
                }
            }
        }
    }

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    solve(&filename).unwrap();
}
