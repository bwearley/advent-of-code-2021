use std::env;
use std::io::{self};

const INITIAL_SPAWN_DAYS: usize = 8;
const RESPAWN_DAYS: usize = 6;
const DAYS_P1: usize = 80;
const DAYS_P2: usize = 256;

fn day06(input: &str) -> io::Result<()> {
    // Input
    let input_str = std::fs::read_to_string(input).unwrap();
    let input_str = input_str.trim();
    let fishes: Vec<_> = input_str.split(',').map(|x| x.parse::<isize>().unwrap()).collect();

    // Initialize
    let mut num_fishes = fishes.len() as isize;
    let mut spawns = vec![0; INITIAL_SPAWN_DAYS+1]; // index is number of days away
    for fish in fishes {
        spawns[fish as usize] += 1;
    }

    // Run
    for day in 0..DAYS_P2 {
        if day == DAYS_P1 { println!("Part 1: {}", num_fishes); } // 351188
        let new_fish = spawns[0];
        num_fishes += new_fish;
        spawns.rotate_left(1);
        spawns[INITIAL_SPAWN_DAYS] = new_fish;
        spawns[RESPAWN_DAYS] += new_fish;
    }
    println!("Part 2: {}", num_fishes); // 1595779846729

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day06(&filename).unwrap();
}
