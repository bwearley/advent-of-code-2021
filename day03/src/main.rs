use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

fn epsilon_from_gamma(gamma: &str) -> String {
    let mut epsilon = String::new();
    for ch in gamma.chars() {
        match ch {
            '0' => epsilon.push_str("1"),
            '1' => epsilon.push_str("0"),
            other => panic!("Unknown digit: {}", other),
        }
    }
    epsilon
}

fn binary_from_string(string: &str) -> isize {
    isize::from_str_radix(&string, 2).unwrap()
}

fn gamma_rate(input: &Vec<String>) -> String {
    let mut gamma_rate = String::new();
    let len = input[0].len();
    for i in 0..len {
        let mut num0 = 0;
        let mut num1 = 0;
        for number in input {
            match number.chars().nth(i).unwrap() {
                '0' => num0 += 1,
                '1' => num1 += 1,
                other => panic!("Unknown digit: {}", other),
            }
        }
        if num0 > num1 {
            gamma_rate.push_str("0");
        } else {
            gamma_rate.push_str("1");
        }
    }
    gamma_rate
}

#[derive(Debug, Clone, Copy)]
enum SystemType {
    OxygenGenerator,
    CO2Scrubber,
}

fn system_rating(input: &Vec<String>, system: SystemType) -> isize {
    let mut inputs = input.clone();
    let len = inputs[0].len();
    for i in 0..len {
        if inputs.len() == 1 { break; }
        let mut num0 = 0;
        let mut num1 = 0;
        for number in &inputs {
            match number.chars().nth(i).unwrap() {
                '0' => num0 += 1,
                '1' => num1 += 1,
                other => panic!("Unknown digit: {}", other),
            }
        }
        let keep = match system {
            SystemType::OxygenGenerator => if num0 > num1 { '0' } else { '1' },
            SystemType::CO2Scrubber =>     if num0 > num1 { '1' } else { '0' },
        };
        inputs.retain(|x| x.chars().nth(i).unwrap() == keep);
    }
    binary_from_string(inputs.iter().next().unwrap())
}

fn day03(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    // Input
    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    
    // Part 1
    let gamma   = gamma_rate(&input);
    let epsilon = epsilon_from_gamma(&gamma);
    let gamma   = binary_from_string(&gamma);
    let epsilon = binary_from_string(&epsilon);
    println!("Part 1: {}", gamma * epsilon); // 3895776

    // Part 2
    let o2  = system_rating(&input, SystemType::OxygenGenerator);
    let co2 = system_rating(&input, SystemType::CO2Scrubber);
    println!("Part 2: {}", o2 * co2); // 7928162

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day03(&filename).unwrap();
}