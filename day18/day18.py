#!/usr/bin/env python3
import sys
from typing import List
from typing import Tuple

EXPLOSION_DEPTH = 4

class TreeNode(object):
    
    def __init__(self):
        self.parent = None
        self.left   = None
        self.right  = None
        self.value  = None
    
    def __str__(self):
        if self.value is not None:
            return f"{self.value}"
        else:
            return f"[{self.left},{self.right}]"  
    
    def __add__(self,other):
        new,_ = parse_number(f"[{self},{other}]")
        return new
    
    def magnitude(self):
        if self.value is not None:
            return self.value
        else:
            return 3 * self.left.magnitude() + 2 * self.right.magnitude()

def get_data(filename: str) -> List[str]:
    lines = []
    with open(filename,'r') as f:
        lines = f.readlines()
    return [l.strip() for l in lines]

def parse_number(input: str, parent: TreeNode = None) -> Tuple[TreeNode,str]:
    new = TreeNode()
    new.parent = parent
    while True:
        char = input[0]
        input = input[1:]
        if char == "[":
            (new.left,remain) = parse_number(input, parent=new)
            input = remain
        elif char == "]":
            return (new,input)
        elif char == ",":
            (new.right,remain) = parse_number(input, parent=new)
            input = remain
        else:
            # Real number
            new.value = int(char)
            return (new,input)
        if len(input) == 0: break
    return (new,input)

def reduce(number: TreeNode):    
    while True:
        exploded = explode(number,0)
        if exploded: continue

        splitted = split(number)
        if splitted: continue

        break

def explode(number: TreeNode, depth: int) -> bool:

    if depth > EXPLOSION_DEPTH:

        right = number.parent.right

        # Ascend tree to the left looking for a branch who's
        # .left is not the current branch
        left_ref = number.parent
        down = number
        while left_ref is not None:
            if left_ref.left == down:
                # continue up left
                down = left_ref
                left_ref = left_ref.parent
            else:
                break
        
        # Ascend tree to the right looking for a branch who's
        # .right is not the current branch
        right_ref = number.parent
        down = number.parent.right
        while right_ref is not None:
            if right_ref.right == down:
                # continue up right
                down = right_ref
                right_ref = right_ref.parent
            else:
                break

        # Descend left tree
        if left_ref is not None:
            left_ref = left_ref.left
            while left_ref is not None:
                try:
                    left_ref.value += number.value
                    break
                except:
                    pass
                left_ref = left_ref.right

        # Descend right tree
        if right_ref is not None:
            right_ref = right_ref.right
            while right_ref is not None:
                try:
                    right_ref.value += right.value
                    break
                except:
                    pass
                right_ref = right_ref.left

        # Explode
        number.parent.left = None
        number.parent.right = None
        number.parent.value = 0
        return True

    else:

        if number.left is not None:
            if explode(number.left, depth+1):  return True
        if number.right is not None:
            if explode(number.right, depth+1): return True
        return False

def split(number: TreeNode) -> bool:

    if number.value is not None and number.value >= 10:
        # Split
        # To split a regular number, replace it with a pair; the left 
        # element of the pair should be the regular number divided by
        # two and rounded down, while the right element of the pair should 
        # be the regular number divided by two and rounded up. For example, 
        # 10 becomes [5,5], 11 becomes [5,6], 12 becomes [6,6], and so on.
        number.left = TreeNode()
        number.left.parent = number
        number.left.value = number.value // 2

        number.right = TreeNode()
        number.right.parent = number
        if number.value % 2 == 0:
            number.right.value = number.value // 2
        else:
            number.right.value = number.value // 2 + 1

        number.value = None

        return True

    else:

        if number.left is not None:
            if split(number.left): return True
        if number.right is not None:
            if split(number.right): return True
        return False

def main():
    input = get_data(sys.argv[1])

    # Parse input
    numbers = []
    for num in input:
        num,_ = parse_number(num)
        numbers.append(num)
    
    # Part 1
    root = numbers[0]
    for i in range(1,len(numbers)):
        root += numbers[i]
        reduce(root)
    print(f"Part 1: {root.magnitude()}") # 4323

    # Part 2
    part2 = 0
    for num1 in numbers:
        for num2 in numbers:
            if num1 == num2: continue
            sum = num1 + num2
            reduce(sum)
            part2 = max(sum.magnitude(),part2)
    print(f"Part 2: {part2}") # 4749

if __name__=="__main__":
    main()