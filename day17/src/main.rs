use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::ops::RangeInclusive;
use std::collections::HashSet;

extern crate regex;
use regex::Regex;

fn step(pos: &mut (i64,i64), vel: &mut (i64,i64)) {
    pos.0 += vel.0;
    pos.1 += vel.1;
    vel.1 += -1;
    vel.0 = std::cmp::max(0,vel.0-1);
}

fn solve(input: &str) -> io::Result<()> {
    // Input
    let input_str = std::fs::read_to_string(input).unwrap();
    let input_str = input_str.trim();

    // target area: x=56..76, y=-162..-134
    let re = Regex::new(r"x=(\d+)\.\.(\d+), y=(\-*\d+)..(\-*\d+)").unwrap();
    let matches = re.captures(input_str).unwrap();
    let target: (i64,i64,i64,i64) = (
        matches[1].parse().unwrap(),
        matches[2].parse().unwrap(),
        matches[3].parse().unwrap(),
        matches[4].parse().unwrap(),
    );
    println!("Target: {:?}", target);
    let xrange: RangeInclusive<i64> = (target.0..=target.1);
    let yrange: RangeInclusive<i64> = (target.2..=target.3);
    
    let xmax = xrange.clone().max().unwrap();
    let ymin = yrange.clone().min().unwrap();

    // Part 1 + 2
    let mut maxy = 0;
    let mut init_vels: HashSet<(i64,i64)> = HashSet::new();
    for xvel in 1..500 {
        for yvel in -1000..10000 {
            let mut pos = (0,0);
            let mut vel = (xvel,yvel);
            let mut thismaxy = 0;
            for s in 0..10000 {
                step(&mut pos,&mut vel);
                thismaxy = std::cmp::max(thismaxy,pos.1);
                if pos.0 > xmax || pos.1 < ymin { break; }
                if xrange.contains(&pos.0) && yrange.contains(&pos.1) {
                    maxy = std::cmp::max(maxy,thismaxy);
                    init_vels.insert((xvel,yvel));
                    break;
                }
            }
        }
    }
    println!("Part 1: {}", maxy); // 13041

    let part2 = init_vels.len();
    println!("Part 2: {}", part2); // 1031


    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    solve(&filename).unwrap();
}
