use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

use intcode::intcode::IntcodeComputer;
extern crate intcode;

fn day07(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let input = input.concat();
    let intcode = input
        .split(',')
        .map(|x| x.parse::<i64>().unwrap())
        .collect::<Vec<i64>>();

    // Initialize & Configure VM
    let mut vm = IntcodeComputer::new(intcode);
    vm.break_outputs = None;
    vm.run();
    vm.stream_ascii(); // Ceci n'est pas une intcode program
    
    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day07(&filename).unwrap();
}