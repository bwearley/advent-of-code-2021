use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::collections::HashMap;
use std::fmt;

const DEBUG: bool = false;

type SeaCucumberMap = HashMap<Point2D,SeaCucumber>;

#[derive(Eq, PartialEq, Hash, Clone)]
struct Point2D {
    x: i64,
    y: i64,
}

#[derive(Clone,Copy,PartialEq)]
enum SeaCucumber {
    East,
    South,
}
impl fmt::Display for SeaCucumber {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            SeaCucumber::East => write!(f, ">"),
            SeaCucumber::South => write!(f, "v"),
        }
    }
}

fn solve(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    // Input
    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    // Build sea cucumber map
    let mut cucumber_map: HashMap<Point2D,SeaCucumber> = HashMap::new();
    for (y,line) in input.iter().enumerate() {
        for (x,ch) in line.chars().enumerate() {
            let (x,y) = (x as i64, y as i64);
            match ch {
                'v' => { cucumber_map.insert(Point2D{x,y},SeaCucumber::South); },
                '>' => { cucumber_map.insert(Point2D{x,y},SeaCucumber::East); },
                '.' => {},
                _   => unreachable!(),
            }
        }
    }

    // Simulate
    let (xmax,ymax) = map_limits(&cucumber_map);
    'main_lp: for step in 0.. {

        // Debug
        if DEBUG { print_map(&cucumber_map, Some(step)); }

        // Consider east moves
        let map_before = cucumber_map.clone();
        let movable_east: Vec<_> = map_before
            .iter()
            .filter(|&(_,c)| *c == SeaCucumber::East)
            .map(|(pt,_)| (pt,cucumber_destination(&pt, SeaCucumber::East, xmax, ymax, &map_before)))
            .filter(|(_,dest)| dest.is_some())
            .collect();

        for (pt,dest) in movable_east {
            if let Some(dest) = dest {
                cucumber_map.remove(pt);
                cucumber_map.insert(dest,SeaCucumber::East);
            }
        }

        // Consider south moves
        let map_before2 = cucumber_map.clone();
        let movable_south: Vec<_> = map_before2
            .iter()
            .filter(|&(_,c)| *c == SeaCucumber::South)
            .map(|(pt,_)| (pt,cucumber_destination(&pt, SeaCucumber::South, xmax, ymax, &map_before2)))
            .filter(|(_,dest)| dest.is_some())
            .collect();

        for (pt,dest) in movable_south {
            if let Some(dest) = dest {
                cucumber_map.remove(pt);
                cucumber_map.insert(dest,SeaCucumber::South);
            }
        }

        // Check for movement
        if map_before == cucumber_map {
            println!("Part 1: {}", step+1); // 509
            break 'main_lp;
        }
    }

    Ok(())
}

fn cucumber_destination(pt: &Point2D, cucumber: SeaCucumber, xmax: i64, ymax: i64, map: &SeaCucumberMap) -> Option<Point2D> {
    let newx = if pt.x+1 <= xmax { pt.x+1 } else { 0 };
    let newy = if pt.y+1 <= ymax { pt.y+1 } else { 0 };
    let dest = match cucumber {
        SeaCucumber::East  => Point2D { x: newx, y: pt.y },
        SeaCucumber::South => Point2D { x: pt.x, y: newy },
    };
    if !map.get(&dest).is_some() {
        return Some(dest);
    }
    None
}

fn map_limits(map: &SeaCucumberMap) -> (i64,i64) {
    (map.keys().map(|k| k.x).max().expect("Failed to determine maximum x-dimension."),
     map.keys().map(|k| k.y).max().expect("Failed to determine maximum y-dimension."))
}

fn print_map(map: &SeaCucumberMap, step: Option<usize>) {
    
    let (xmax,ymax) = map_limits(&map);
    if step.is_some() {  println!("Step: {}", step.unwrap()); }        
    
    for y in 0..=ymax {
        for x in 0..=xmax {
            match map.get(&Point2D{x,y}) {
                Some(cuke) => print!("{}", cuke),
                None       => print!("."),
            }
        }
        println!();
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    solve(&filename).unwrap();
}
