use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

// Count number of matches second string has to characters in first string
fn num_matches(first: &String, second: &String) -> usize {
    first.chars().filter(|c| second.contains(&c.to_string())).count()
}

struct PuzzleInput {
    signals: Vec<String>,
    outputs: Vec<String>,
}
impl PuzzleInput {
    pub fn output(&self) -> usize {
        let signal1 = self.signals.iter().filter(|s| s.len() == 2).map(|s| s.to_string()).next().unwrap();
        let signal4 = self.signals.iter().filter(|s| s.len() == 4).map(|s| s.to_string()).next().unwrap();
      //let signal7 = self.signals.iter().filter(|s| s.len() == 3).map(|s| s.to_string()).next().unwrap();
      //let signal8 = self.signals.iter().filter(|s| s.len() == 7).map(|s| s.to_string()).next().unwrap();

        let mut output = String::new();
        for o in &self.outputs {
            match (o.len(),num_matches(&o,&signal1),num_matches(&o,&signal4)) {
                (6,2,3) => output.push_str("0"),
                (2,_,_) => output.push_str("1"),
                (5,1,2) => output.push_str("2"),
                (5,2,3) => output.push_str("3"),
                (4,_,_) => output.push_str("4"),
                (5,1,3) => output.push_str("5"),
                (6,1,3) => output.push_str("6"),
                (3,_,_) => output.push_str("7"),
                (7,_,_) => output.push_str("8"),
                (6,_,4) => output.push_str("9"),
                (_,_,_) => panic!("Unknown failure for signal: {}",&o),
            }
        }
        output.parse::<usize>().unwrap()
    }
}
impl From<&String> for PuzzleInput {
    fn from(s: &String) -> Self {
        let parts = s.split(" | ").collect::<Vec<_>>();
        let signals = parts[0].split_ascii_whitespace().map(|x| x.to_string()).collect();
        let outputs = parts[1].split_ascii_whitespace().map(|x| x.to_string()).collect();
        Self {
            signals: signals,
            outputs: outputs,
        }
    }
}

fn day08(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    // Input
    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let inputs: Vec<_> = input.iter().map(PuzzleInput::from).collect();

    // Part 1
    let part1 = inputs
        .iter()
        .map(|x| {
            x.outputs
                .iter()
                .filter(|o| match o.len() { 2|4|3|7 => true, _ => false})
                .count()
        })
        .sum::<usize>();
    println!("Part 1: {}", part1); // 488

    // Part 2
    let part2 = inputs.iter().map(|x| x.output()).sum::<usize>();
    println!("Part 2: {}", part2); // 1040429

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day08(&filename).unwrap();
}
